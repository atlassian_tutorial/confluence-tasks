package com.atlassian.confluence.tutorials.tasks.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;

/**
 * Action that serves JavaScript that calls the REST API for tasks.
 */
public class TasksRestApiExampleAction extends ConfluenceActionSupport
{
    @Override
    public String execute() throws Exception
    {
        return SUCCESS;
    }
}
