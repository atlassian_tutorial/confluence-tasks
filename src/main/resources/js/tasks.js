AJS.toInit(function($) {
    var clearMessages = function() {
        $("#aui-message-bar").empty();
    },

    fetchAndDisplayTasks = function() {
        $tasksContainer = $("#rest-tasks");
        $tasksContainer.html("Loading...");
        $.ajax({
            type: 'GET',
            url: AJS.contextPath() + '/rest/mywork/latest/task',
            contentType:'application/json'
        }).done(function(tasks) {
            var $tasksList = $("<ul />");
            $tasksContainer.html($tasksList);
            for (var i = 0; i < tasks.length; ++i) {
                var task = tasks[i];
                if (task.application != "com.atlassian.confluence.confluence-tasks")
                    continue;

                var $taskItem = $("<li/>");
                var $taskTitleSpan = $("<span/>");
                $taskTitleSpan.text(task.title);
                $taskItem.append($taskTitleSpan);
                $taskItem.append($('<a href="#delete-task" data-task-id="' + task.id + '">Delete</a>'));
                $tasksList.append($taskItem);
            }
            if ($tasksList.find("li").length == 0) {
                $tasksContainer.html("<p>No tasks have been created yet. Fill the form below to create a task.</p>");
            }
        }).fail(function() {
            clearMessages();
            AJS.messages.error("#aui-message-bar", {
               body: "<p>Failed to fetch tasks.</p>",
               closeable: true,
               shadowed: true
            });
        });
    },

    deleteTask = function(taskId) {
        $.ajax({
            type: 'DELETE',
            url: AJS.contextPath() + '/rest/mywork/latest/task/' + taskId,
            contentType:'application/json'
        }).done(function(task) {
            clearMessages();
            AJS.messages.success("#aui-message-bar", {
               body: "<p>Task deleted successfully.</p>",
               closeable: true,
               shadowed: true
            });
            fetchAndDisplayTasks();
        }).fail(function() {
            clearMessages();
            AJS.messages.error("#aui-message-bar", {
               body: "<p>Failed to delete task.</p>",
               closeable: true,
               shadowed: true
            });
        });
    },

    createTaskFromForm = function() {
        $.ajax({
           type: 'POST',
           url: AJS.contextPath() + '/rest/mywork/latest/task',
           dataType: "json",
           contentType:'application/json',
           data: JSON.stringify({
               //"applicationLinkId": "bf13be6c-926b-318e-95cc-99dc04f8597e",
               "application": "com.atlassian.confluence.confluence-tasks",
//                "entity": "page",
//                "globalId": "12345",
//                "item": {
//                    "title": "This is the item title"
//                    "iconUrl": "",
//                    "url": "",
//                },
               "title": $("#task-title-field").val(),
               "notes": $("#task-notes-field").val(),
               "status": $("#task-status-checkbox:checked").length == 0? 'TODO' : 'DONE'
//                   "created": 0,
//                   "updated": 0
//                   "metadata": {
//                       "user": "This is the user metadata"
//                   }
           })
       }).done(function(task) {
           clearMessages();
           AJS.messages.success("#aui-message-bar", {
              title:"New task created successfully.",
              body: "<p>" + task.title + "</p>",
              closeable: true,
              shadowed: true
           });
           fetchAndDisplayTasks();
       }).fail(function() {
           clearMessages();
           AJS.messages.error("#aui-message-bar", {
              body: "<p>Failed to create task.</p>",
              closeable: true,
              shadowed: true
           });
       });
    };

    $("#add-task-rest-form").submit(function(e) {
        e.preventDefault();
        createTaskFromForm();
    });

    $("#rest-tasks a").live("click", function(e) {
        var taskId = $(e.target).attr("data-task-id");
        deleteTask(taskId);
    });

    // Fetch tasks on page load
    fetchAndDisplayTasks();
});